package com.example.alexandrfominov.baseapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.alexandrfominov.baseapp.Data;
import com.example.alexandrfominov.baseapp.R;
import com.example.alexandrfominov.baseapp.User;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class UserInfoFragment extends Fragment
{

    Button showComments;
    Button showAlbums;
    FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        int position = this.getArguments().getInt ("position");
        User user = Data.getInstance().getUsers().get(position);
        fragmentManager = getActivity().getSupportFragmentManager();

        View rootView = inflater.inflate(R.layout.user_info_fragment, container, false);

        showComments = (Button) rootView.findViewById(R.id.showComments);
        showAlbums = (Button) rootView.findViewById(R.id.showAlbums);

        TextView id = (TextView) rootView.findViewById(R.id.id);
        id.setText("id: " + user.getId());

        TextView name = (TextView) rootView.findViewById(R.id.name);
        name.setText("name: " + user.getName());

        TextView username = (TextView) rootView.findViewById(R.id.username);
        username.setText("username: " + user.getUsername());

        TextView email = (TextView) rootView.findViewById(R.id.email);
        email.setText("email: " + user.getEmail());

        TextView address = (TextView) rootView.findViewById(R.id.address);
        address.setText("address: " + user.getAddress().getCity() + ", " + user.getAddress().getStreet());

        showComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Bundle bundle = new Bundle();
                bundle.putInt("position", 1);

                UserCommentsFragment commentsFragment = new UserCommentsFragment();
                commentsFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.frame2, commentsFragment);

                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        showAlbums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Bundle bundle = new Bundle();
                bundle.putInt("position", 0);
                UsersAlbumsFragment albumFragment = new UsersAlbumsFragment();
                albumFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.frame2, albumFragment);

                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return rootView;

    }
}

