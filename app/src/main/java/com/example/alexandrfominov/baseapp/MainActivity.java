package com.example.alexandrfominov.baseapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alexandrfominov.baseapp.adapter.UserAdapter;
import com.example.alexandrfominov.baseapp.network.FakeServer;
import com.example.alexandrfominov.baseapp.network.Rest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private String url = "http://jsonplaceholder.typicode.com";

    FakeServer server;
    private List<Photo> photos;
    private List<User> users;

    ListView userList;
    UserAdapter adapter;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor e;

    Gson gson;

    Type arrayOfUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        arrayOfUsers = new TypeToken<List<User>>(){}.getType();

        sharedPreferences = getPreferences(MODE_PRIVATE);
        e = sharedPreferences.edit();

        gson = new GsonBuilder().create();

        Button loadButton = (Button) findViewById(R.id.load);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usersArray = sharedPreferences.getString("users", null);
                if (usersArray == null)
                {
                    getUsers();
                } else
                {
                    List<User> users = gson.fromJson(usersArray, arrayOfUsers);
                    Data.getInstance().setUsers(users);
                    drawUsers();
                }
            }
        });

        Button saveButton = (Button) findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<User> u = Data.getInstance().getUsers();
                if (u != null)
                {
                    Toast.makeText(getApplicationContext(), "Data saved", Toast.LENGTH_LONG).show();
                    String newUsers = gson.toJson(u);
                    e.putString("users", newUsers);
                    e.commit();
                } else
                {
                    Toast.makeText(getApplicationContext(), "No data to save", Toast.LENGTH_LONG).show();
                }
            }
        });

      //  getUsers();

    }

    void getPhotos ()
    {
        Rest.getInstance().getPhotos().enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                photos = response.body();
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "something went wrong 1 ", Toast.LENGTH_SHORT).show ();
            }
        });
    }

    void getUsers()
    {
        Rest.getInstance().getUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                Data.getInstance().setUsers(response.body());

                e.putString("users", gson.toJson (response.body()));

             /*   for (int i = 0; i < response.body().size(); i++)
                {

                    e.putString("user_" + i, gson.toJson (response.body().get(i)));
                } */

          //      Log.d("dsf", gson.toJson (response.body().get(0)));

                drawUsers ();
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show ();
            }
        });
    }

    void drawUsers ()
    {
        userList = (ListView) findViewById(R.id.listview);
        adapter = new UserAdapter(Data.getInstance().getUsers(), this);
        userList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent loadInfoPage = new Intent(getApplicationContext(), UserInfo.class);
                loadInfoPage.putExtra("position", position);
                startActivity (loadInfoPage);
            }
        });
    }



}