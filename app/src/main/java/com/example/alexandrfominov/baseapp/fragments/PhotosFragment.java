package com.example.alexandrfominov.baseapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexandrfominov.baseapp.Data;
import com.example.alexandrfominov.baseapp.Photo;
import com.example.alexandrfominov.baseapp.R;
import com.example.alexandrfominov.baseapp.adapter.PhotosAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandrfominov on 01.06.16.
 */
public class PhotosFragment extends Fragment
{

    protected View mRootView;
    protected RecyclerView rView;
    protected PhotosAdapter adapter;
    int position;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.photos_fragment_layout, container, false);

        rView = (RecyclerView) mRootView.findViewById(R.id.photorv);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        rView.setLayoutManager(manager);

        position = this.getArguments().getInt("position");

        ArrayList<Photo> photos = new ArrayList<>();
        List<Photo> allPhotos = Data.getInstance().getPhotos();
        for (int i = 0; i < allPhotos.size(); i++)
        {
            Photo photo = allPhotos.get (i);
            if (photo.getAlbumId() == position)
            {
                photos.add(photo);
            }
        }

        adapter = new PhotosAdapter(photos, getActivity());
        rView.setAdapter(adapter);

        return mRootView;
    }
}
