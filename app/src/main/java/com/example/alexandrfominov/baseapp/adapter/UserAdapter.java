package com.example.alexandrfominov.baseapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.alexandrfominov.baseapp.R;
import com.example.alexandrfominov.baseapp.User;

import java.util.List;

/**
 * Created by alexandrfominov on 30.05.16.
 */
public class UserAdapter extends BaseAdapter {

    List<User> users;
    Context context;

    public UserAdapter (List<User> users, Context context)
    {
        this.users = users;
        this.context = context;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.userlayout, null);
        }

        TextView textView = (TextView) view.findViewById (R.id.name);
        textView.setText(users.get(position).getName());
        return view;
    }

}
