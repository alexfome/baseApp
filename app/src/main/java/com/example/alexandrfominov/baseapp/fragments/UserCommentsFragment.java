package com.example.alexandrfominov.baseapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alexandrfominov.baseapp.Comment;
import com.example.alexandrfominov.baseapp.R;
import com.example.alexandrfominov.baseapp.adapter.CommentsAdapter;
import com.example.alexandrfominov.baseapp.network.Rest;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class UserCommentsFragment extends Fragment {

    private int position;
    private ListView commentsListView;
    private CommentsAdapter adapter;

    private View mRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.user_comments_fragment, container, false);

        position = this.getArguments().getInt("position");

        Rest.getInstance().getComments().enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                showUsersComments(response.body());
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Toast.makeText(getActivity(), "failed", Toast.LENGTH_LONG).show ();
            }
        });
        return mRootView;

    }

    private void showUsersComments(List<Comment> comments) {
        List<Comment> userComments = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            if (position == comments.get(i).getId()) {
                userComments.add(comments.get(i));
            }
        }

        commentsListView = (ListView) mRootView.findViewById(R.id.comments);
        adapter = new CommentsAdapter(userComments, getActivity());
        commentsListView.setAdapter(adapter);

    }

}
