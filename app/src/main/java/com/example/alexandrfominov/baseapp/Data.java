package com.example.alexandrfominov.baseapp;

import java.util.List;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class Data {

    private static Data usersSingleton = null;
    public List<User> users;
    public List<Photo> photos;

    public static Data getInstance ()
    {
        if (usersSingleton == null)
        {
            usersSingleton = new Data();
        }
        return usersSingleton;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }
}
