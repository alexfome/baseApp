package com.example.alexandrfominov.baseapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.alexandrfominov.baseapp.Album;
import com.example.alexandrfominov.baseapp.Data;
import com.example.alexandrfominov.baseapp.Photo;
import com.example.alexandrfominov.baseapp.R;
import com.example.alexandrfominov.baseapp.adapter.AlbumAdapter;
import com.example.alexandrfominov.baseapp.network.Rest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class UsersAlbumsFragment extends Fragment
{

    View mRootView;
    int position;
    List<Album> albumList;
    RecyclerView rView;
    AlbumAdapter adapter;
    private boolean needSetup = false;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.album_fragment, container, false);

        rView = (RecyclerView) mRootView.findViewById(R.id.rv);
        GridLayoutManager manager = new GridLayoutManager (getActivity(), 3, GridLayoutManager.VERTICAL, false);
        rView.setLayoutManager(manager);

        position = this.getArguments().getInt("position");

        loadPhotos();
        loadAlbums();
        return mRootView;
    }

    private void loadPhotos()
    {
        Rest.getInstance().getPhotos().enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                Data.getInstance().setPhotos(response.body());
                if (needSetup) {
                    setAlbumAdapter(albumList);
                }
                needSetup = true;
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Toast.makeText(getActivity(), "failed to load photos", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void loadAlbums ()
    {
        Rest.getInstance().getAlbums().enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                albumList = response.body();
                if (needSetup) {
                    setAlbumAdapter(albumList);
                }
                needSetup = true;
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {

            }
        });
    }

    private void setAlbumAdapter(List<Album> albumList) {
        adapter = new AlbumAdapter(albumList, getActivity());
        adapter.setListner(new AlbumAdapter.OnClickListner() {
            @Override
            public void onItemclick(int position) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

                PhotosFragment pf = new PhotosFragment();
                Bundle b = new Bundle();
                b.putInt ("position", position);
                pf.setArguments(b);
                transaction.replace(R.id.frame2, pf);

                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        rView.setAdapter(adapter);
    }

}
