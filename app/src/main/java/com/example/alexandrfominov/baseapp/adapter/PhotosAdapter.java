package com.example.alexandrfominov.baseapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.alexandrfominov.baseapp.Data;
import com.example.alexandrfominov.baseapp.Photo;
import com.example.alexandrfominov.baseapp.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by alexandrfominov on 01.06.16.
 */
public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder>
{

    protected ArrayList<Photo> photos;
    protected Context context;


    public PhotosAdapter (ArrayList<Photo> photos, Context context)
    {
        this.photos = photos;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        protected ImageView image;
        public ViewHolder (View itemView)
        {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photo_layout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage(Data.getInstance().getPhotos().get(position).getUrl(), holder.image);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

}
