package com.example.alexandrfominov.baseapp.network;

import com.example.alexandrfominov.baseapp.Album;
import com.example.alexandrfominov.baseapp.Comment;
import com.example.alexandrfominov.baseapp.Photo;
import com.example.alexandrfominov.baseapp.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class Rest {
    private String url = "http://jsonplaceholder.typicode.com";
    private Rest rest;
    private FakeServer server;

    private Rest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        server = retrofit.create(FakeServer.class);
    }

    public static Rest getInstance(){
        return InstanceHolder.INSTANCE;
    }

    public Call<List<Photo>> getPhotos(){
        return server.getPhotos();
    }

    public Call<List<User>> getUsers(){
        return server.getUsers();
    }

    public Call<List<Album>> getAlbums(){
        return server.getAlbums();
    }

    public Call<List<Comment>> getComments(){
        return server.getComments();
    }

    public Call<Comment> makeComment (Comment c) { return server.makePost(c); }

    public Call<Comment> makePut (Comment c) { return server.makePut(c); }

    public Call<Comment> makePatch (Comment c) { return server.makePatch(c); }

    private static class InstanceHolder {
        private static Rest INSTANCE = new Rest();
    }
}
