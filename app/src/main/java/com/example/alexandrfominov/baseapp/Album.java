package com.example.alexandrfominov.baseapp;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class Album
{

    private int userId;
    private int id;
    private String title;
    private String firstPhoto;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
