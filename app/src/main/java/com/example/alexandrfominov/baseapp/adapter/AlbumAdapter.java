package com.example.alexandrfominov.baseapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alexandrfominov.baseapp.Album;
import com.example.alexandrfominov.baseapp.Data;
import com.example.alexandrfominov.baseapp.Photo;
import com.example.alexandrfominov.baseapp.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    private List<Album> albums;
    private Context context;
    private List<Photo> photos;
    private OnClickListner listner;


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView textView;
        ImageView image;

        public TextView albumNum;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.albumNum);
            image = (ImageView) itemView.findViewById(R.id.image);
        }

    }

    public void setListner(OnClickListner listner) {
        this.listner = listner;
    }

    public AlbumAdapter (List<Album> albums, Context context)
    {
        this.albums = albums;
        this.context = context;
        photos = Data.getInstance().getPhotos();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.albumlayout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(Integer.toString(position));
        Photo firstPhoto =  photos.get(position + 1);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.onItemclick(position);
            }
        });

        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage(firstPhoto.getUrl(), holder.image);
    }

    @Override
    public int getItemCount() {

        return albums.size();
    }

    public interface OnClickListner{
        void onItemclick(int position);
    }

}
