package com.example.alexandrfominov.baseapp;

/**
 * Created by alexandrfominov on 30.05.16.
 */
public class Comment {
    int postId, id, userId;
    String name, email, body, title;

    public Comment (int id, String name, String email, String body, String title)
    {
        this.id = id;
        this.name = name;
        this.email = email;
        this.body = body;
        this.title = title;
    }

    public Comment(int id, String body, String title) {
        this.title = title;
        this.body = body;
        this.id = id;
    }

    public Comment(int id, String body, String title, int userId) {
        this.title = title;
        this.body = body;
        this.id = id;
        this.userId = userId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
