package com.example.alexandrfominov.baseapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.alexandrfominov.baseapp.fragments.UserInfoFragment;

public class UserInfo extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        position = getIntent().getExtras().getInt("position");

        fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        UserInfoFragment infoFragment = new UserInfoFragment();
        infoFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.frame, infoFragment);

        fragmentTransaction.commit();


    }



}
