package com.example.alexandrfominov.baseapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.alexandrfominov.baseapp.Comment;
import com.example.alexandrfominov.baseapp.R;

import java.util.List;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class CommentsAdapter extends BaseAdapter {

    Context context;
    List<Comment> userComments;

    public CommentsAdapter(List<Comment> userComments, Context context)
    {
        this.userComments = userComments;
        this.context = context;
    }

    @Override
    public int getCount() {
        return userComments.size();
    }

    @Override
    public Object getItem(int position) {
        return userComments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null)
        {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.commentlayout, null);
        }
            TextView t = (TextView) v.findViewById(R.id.commentBody);
            t.setText(userComments.get(position).getBody());

        return v;
    }
}
