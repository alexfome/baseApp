package com.example.alexandrfominov.baseapp.network;

import com.example.alexandrfominov.baseapp.Album;
import com.example.alexandrfominov.baseapp.Comment;
import com.example.alexandrfominov.baseapp.Photo;
import com.example.alexandrfominov.baseapp.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by alexandrfominov on 30.05.16.
 */
public interface FakeServer {
    @GET("/photos")
    Call<List<Photo>> getPhotos();

    @GET("/users")
    Call<List<User>> getUsers();

    @GET("/comments")
    Call<List<Comment>> getComments();

    @GET("/albums")
    Call<List<Album>> getAlbums();

    @POST("/posts")
    Call<Comment> makePost(@Body com.example.alexandrfominov.baseapp.Comment comment);

    @PUT("/posts/6")
    Call<Comment> makePut(@Body com.example.alexandrfominov.baseapp.Comment comment);

    @PATCH("/posts/1")
    Call<Comment> makePatch(@Body com.example.alexandrfominov.baseapp.Comment comment);
}
