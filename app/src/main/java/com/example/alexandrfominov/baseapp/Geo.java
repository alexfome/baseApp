package com.example.alexandrfominov.baseapp;

/**
 * Created by alexandrfominov on 31.05.16.
 */
public class Geo
{
    private String lat;
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
